#include "stdafx.h"
#include "CppUnitTest.h"
#include "../rotate.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(RotateTests)
	{
	public:
		
		TEST_METHOD(RotateSimple)
		{
			//ARRANGE
			std::string test_str = "12345678";
			const std::string expected_result = "67812345";

			//ACT
			experiments::rotate(test_str, 3);

			//ASSERT
			Assert::AreEqual(expected_result, test_str);
		}

		TEST_METHOD(RotateSimpleBiggerThenStr)
		{
			//ARRANGE
			std::string test_str = "12345678";
			const std::string expected_result = "67812345";

			//ACT
			experiments::rotate(test_str, test_str.size() + 3);

			//ASSERT
			Assert::AreEqual(expected_result, test_str);
		}

		TEST_METHOD(test_rotate_full_size)
		{
			//ARRANGE
			std::string test_str = "12345678";
			const std::string expected_result = "12345678";

			//ACT
			experiments::rotate(test_str, test_str.size());

			//ASSERT
			Assert::AreEqual(expected_result, test_str);
		}

		TEST_METHOD(test_rotate_backwards)
		{
			//ARRANGE
			std::string test_str = "12345678";
			const std::string expected_result = "45678123";

			//ACT
			experiments::rotate(test_str, -3);

			//ASSERT
			Assert::AreEqual(expected_result, test_str);
		}

		TEST_METHOD(test_rotate_backwards_number_bigger_then_size)
		{
			//ARRANGE
			std::string test_str = "12345678";
			const std::string expected_result = "45678123";

			//ACT
			experiments::rotate(test_str, -static_cast<int>(test_str.size()) - 3);

			//ASSERT
			Assert::AreEqual(expected_result, test_str);
		}

		TEST_METHOD(test_rotate_zero_number)
		{
			//ARRANGE
			std::string test_str = "12345678";
			const std::string expected_result = "12345678";

			//ACT
			experiments::rotate(test_str, 0);

			//ASSERT
			Assert::AreEqual(expected_result, test_str);
		}

		TEST_METHOD(test_rotate_zero_string)
		{
			//ARRANGE
			std::string test_str = "";
			const std::string expected_result = "";

			//ACT
			experiments::rotate(test_str, 150);

			//ASSERT
			Assert::AreEqual(expected_result, test_str);
		}

	};
}