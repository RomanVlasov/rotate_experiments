#include "stdafx.h"
#include "CppUnitTest.h"
#include "../stepanov_lib.hpp"
#include <vector>
#include <algorithm>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {

			template<>
			static std::wstring ToString<std::vector<int>>(const std::vector<int> & data)
			{
				std::wstring result;
				std::for_each(data.begin(), data.end(),
					[&result](const auto& a_Data) 
					{
						result += (result.empty() ? L"" : L",") + std::to_wstring(a_Data);
					});
				return result;
			}

		}
	}
}

namespace UnitTests
{		
	TEST_CLASS(StepanovTests)
	{
	public:
		TEST_METHOD(SwapRangesSimple)
		{
			//ARRANGE
			auto test_data_1 = std::vector<int>{10, 20, 45, 67};
			auto test_data_2 = std::vector<int>{15, 25};

			//ACT
			stepanov::swap_ranges(test_data_1.begin(), test_data_1.end(), test_data_2.begin(), test_data_2.end());

			//ASSERT
			Assert::AreEqual({ 15, 25, 45, 67 }, test_data_1);
			Assert::AreEqual({ 10, 20 }, test_data_2);
		}

		TEST_METHOD(SwapRangesReverse)
		{
			//ARRANGE
			auto test_data_1 = std::vector<int>{ 10, 20, 45, 67 };
			auto test_data_2 = std::vector<int>{ 15, 25 };

			//ACT
			stepanov::swap_ranges(test_data_2.begin(), test_data_2.end(), test_data_1.begin(), test_data_1.end());

			//ASSERT
			Assert::AreEqual({ 15, 25, 45, 67 }, test_data_1);
			Assert::AreEqual({ 10, 20 }, test_data_2);
		}
	};
}