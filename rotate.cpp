#include "rotate.hpp"
#include "stepanov_lib.hpp"


namespace experiments
{
	namespace internal
	{
		void rotate_simple(std::string& a_Source, int number)
		{
			if (a_Source.empty())
			{
				return;
			}
			
			while (number < 0)
			{
				number += static_cast<int>(a_Source.size());
			}

			if (number == 0)
			{
				return;
			}
			
			number = number % a_Source.size();
			a_Source = a_Source.substr(a_Source.size() - number) + a_Source.substr(0, a_Source.size() - number);
		}

		void rotate_ranges(std::string& a_Source, int number)
		{
			if (a_Source.empty())
			{
				return;
			}

			while (number < 0)
			{
				number += static_cast<int>(a_Source.size());
			}

			if (number == 0)
			{
				return;
			}

			number = number % a_Source.size();
			stepanov::rotate_ranges(a_Source.begin(), a_Source.end() - number, a_Source.end());
		}
	}
	
	void rotate(std::string& a_Source, int number)
	{
		//return internal::rotate_simple(a_Source, number);
		return internal::rotate_ranges(a_Source, number);
	}
}