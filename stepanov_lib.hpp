#pragma once


namespace stepanov
{
	template <typename TIter>
	auto swap_ranges(TIter a_FirstStart, TIter a_FirstEnd, TIter a_SecondStart, TIter a_SecondEnd) 
		-> decltype(std::make_pair(a_FirstStart, a_SecondStart))
	{
		auto first = a_FirstStart;
		auto second = a_SecondStart;
		while (first != a_FirstEnd && second != a_SecondEnd)
		{
			std::swap(*first, *second);
			++first;
			++second;
		}
		return {first, second};
	}


	template <typename TIter>
	void rotate_ranges(TIter a_BeginIter, TIter a_MiddleIter, TIter a_EndIter)
	{
		if (a_BeginIter == a_MiddleIter || a_MiddleIter == a_EndIter)
		{
			return;
		}

		auto first = a_BeginIter;
		auto second = a_MiddleIter;
		auto first_range_ended = bool{ false };
		auto second_range_ended = bool{ false };

		do
		{
			auto iter_pair = swap_ranges(first, second, second, a_EndIter);

			first_range_ended = (iter_pair.first == second);
			second_range_ended = (iter_pair.second == a_EndIter);

			if (first_range_ended)
			{
				first = second;
				second = iter_pair.second;
			}
			else
			{
				first = iter_pair.first;
			}

		} while (!first_range_ended || !second_range_ended);

	}
}
